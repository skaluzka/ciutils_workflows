#!/bin/env python2

from setuptools import setup

setup(
    name='ciutils_workflows',
    version='0.1.0',
    description='',
    zip_safe=False,
    packages=['ciutils_workflows'],
    install_requires=[
       'ciutils_toolbox',
    ],
    dependency_links=[
      'https://bitbucket.org/skaluzka/ciutils_toolbox/get/master.tar.gz#egg=ciutils_toolbox',
    ],
)

